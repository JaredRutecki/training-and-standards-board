# Illinois Law Enforcement Training and Standards Board Records

This dataset is a record maintained by the [Illinois Law Enforcement Training and Standards Board (ILETSB)](https://www.ptb.illinois.gov) showing work history for all officers who have completed all required training. 

ILETSB is the state agency mandated to promote and maintain a high level of professional standards for law enforcement and correctional officers. Its purpose is to promote and protect citizen health, safety and welfare by encouraging municipalities, counties, park districts, State-controlled universities, colleges, public community colleges, and other local governmental agencies of this state and participating State agencies in their efforts to upgrade and maintain a high level of training and standards for law enforcement personnel.

This information is taken from the Form E, a document that gives an officer's name, start/end date and various other pieces on information, by department. 

The fields are broken up as follows: Police Training Board Identification Number, Last name, First Name, Middle Name, Officer Race, Department Name, Employment class description, Hired Date, Termination Date and Birth Year. The later files also have prior agency and reason for separation fields, though not for all officers.

The PTBID (Police Training Board Identification Number) is a unique ID for individual officers. The race ID is reported by officers. Categories are African American/Black, Caucasian/White, Asian/Oriental and Native American. The departments include all public and private police agences recognized in the state.

There are three employment classes - full time, part time and auxiliary - for police officers. Full- and part-time officers are required to complete a full academy course load. Auxiliary officers [are only required to complete the shooting part of training](https://www.bettergov.org/news/auxiliary-officers-pose-risks-in-illinois-towns/), and their responsibilities are usually limited. The latest set includes classes for officers assigned to state's attorney offices, coroners, correctional, and an undefined other category.

This data was originally obtained in October 2017, and is current to that date. The [most recent file is the latest upload](https://gitlab.com/ChicagoDataCooperative/training-and-standards-board/blob/master/21-348_-_11-25-20_Form_Es.xlsx), and it was obtained in November of 2020.

The [2021 request](https://gitlab.com/ChicagoDataCooperative/training-and-standards-board/-/blob/master/2021-request.csv) is here, the [2020 request](https://gitlab.com/ChicagoDataCooperative/training-and-standards-board/-/blob/master/21-348_-_11-25-20_Form_Es.xlsx) is here, [2019 request](https://gitlab.com/ChicagoDataCooperative/training-and-standards-board/-/blob/master/All_Form_Es_with_Race-2019__1_.xlsx) is here, the [2018 request](https://gitlab.com/ChicagoDataCooperative/training-and-standards-board/-/blob/master/All_Form_Es_with_Race.xlsx) is here and the 2017 request [is here](https://gitlab.com/ChicagoDataCooperative/training-and-standards-board/-/blob/master/AllOfficers-EmpHist-10022017.xlsx).
